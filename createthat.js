window.createthat = {};
var usedTropes = [];
var usedTitles = [];
var currentTitle;
var currentTropes = new Array(3);
var timer = 90;
var timerOn = false;

createthat.init = function (type) {
    $.getJSON( type + ".json", function( json ) {
        createthat.data = json;
        createthat.render_generate();
    } );
    createthat.setTimer( 90 );
    // Ewww special casing
    if ( type == "hentai" ) {
        document.title = "Create that Hentai!";
    }
}

$( document ).ready( function ( type, content ) { 
    // Init Buttons
    $('#generate').click(function(){createthat.generate( $('#tropes-count').val() );});
    $('#render-generate').click(function(){createthat.render_generate();});
    $('#timer-up').click(function(){createthat.adjustTimer(15);});
    $('#timer-down').click(function(){createthat.adjustTimer(-15);});
    $('#timer-toggle').click(function(){createthat.toggleTimer();});
    $('#timer-reset').click(function(){createthat.setTimer(90);});
    $('#start-game').click(function(){createthat.init( $('#game-type').val() )});
} );

createthat.generate = function ( trope_count ) {
    var tropes = [];
    for ( i = 1; i <= trope_count; i++ ) {
        tropes.push( createthat.getTrope() );
    }

    var title = createthat.getTitle();

    var textFill = "";

    textFill += title + "\n\n";

    $(tropes).each(function ( key, trope ) { textFill += trope + "\n" } );
    textFill += "\n";

    words.textContent = textFill;
    $('#generate-settings').hide();
    $('#results').show();
}

createthat.render_generate = function () {
    $('#results').hide();
    $('#type').hide();
    $('#generate-settings').show();
};

createthat.getTrope = function () {
    if ( createthat.data.tropes.length === 0 ) {
        createthat.data.tropes = usedTropes;
        usedTropes = [];
        console.log( 'reset' );
    }
    var trope_id = Math.floor( Math.random() * createthat.data.tropes.length );
    var trope = createthat.data.tropes[trope_id];
    usedTropes.push(createthat.data.tropes.splice( trope_id, 1 )[0]);
 
    console.log( trope );
    return trope;
}

createthat.getTitle = function () {
    var title_id = Math.floor( Math.random() * createthat.data.titles.length );
    var title = createthat.data.titles[title_id];
    usedTitles.push(createthat.data.titles.splice( title_id, 1 )[0]);

    console.log( title );
    return title;
}

function update() {
    document.getElementById('timer').value = timer;
}

createthat.toggleTimer = function () {
    timerOn = !timerOn;
    if (timerOn) {
        setTimeout(tick, 1000);
    }
    update();
}

function tick() {
    if (timerOn) {
        createthat.adjustTimer(-1);
        setTimeout(tick, 1000);
    }
    if (timer <= 0) {
        timerOn = false;
        beep(1000, 1);
    }
}

createthat.adjustTimer = function (time) {
    timer += time;
    update();
}

createthat.setTimer = function (time) {
    timer = time;
    update();
}

var beep = (function () {
    var ctx = new(window.AudioContext || window.webkitAudioContext);
    return function (duration, type, finishedCallback) {

        duration = +duration;

        // Only 0-4 are valid types.
        type = (type % 5) || 0;

        if (typeof finishedCallback != "function") {
            finishedCallback = function () {};
        }

        var osc = ctx.createOscillator();

        osc.type = type;

        osc.connect(ctx.destination);
        osc.start(0);
        
        setTimeout(function () {
            osc.stop(0);
            finishedCallback();
        }, duration);

    };
})();

window.location.hash = '';
window.scroll(0,0);
